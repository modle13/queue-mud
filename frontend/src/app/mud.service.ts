import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import {RESPONSES_URL, COMMAND_URL} from './env';
import {Command} from './mud/command.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class MudService {

  constructor(private http: HttpClient) {
  }

  private static _handleError(err: HttpErrorResponse | any) {
    console.log("got some problems, Bob", err);
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }

  // GET list of results
  getResponses(): Observable<any> {
    return this.http
      .get(`${RESPONSES_URL}`)
      .catch(MudService._handleError);
  }

  /** POST: submit a new command to the server */
  addCommand(thing: Command): Observable<any> {
    console.log("adding command " + thing)
    return this.http
      .post<Command>(`${COMMAND_URL}`, JSON.stringify(thing), httpOptions)
      .catch(MudService._handleError);
  }
}

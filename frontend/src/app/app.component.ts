import {Component, OnInit} from '@angular/core';
import {MudService} from './mud.service';
import {Command} from './mud/command.model';
import {Response} from './mud/response.model';
import {timer} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  responseList: Response[];

  public timer$ = timer(0, 10000);

  constructor(private mudService: MudService) {
    // make call to read from responses queue here, on a timer
    this.timer$.subscribe(tick => {
      this.getResponses();
    })
  }

  ngOnInit() {
    this.getResponses();
  }

  getResponses(): void {
    this.mudService
      .getResponses()
      .subscribe(res => this.responseList = res);
    console.log("responseList is", this.responseList);
  }

  // don't forget to subscribe! or the request doesn't get made
  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    var theThing = new Command(name);
    console.log("adding a thing", theThing);
    this.mudService
      .addCommand(theThing)
      .subscribe(res => console.log(res));
  }
}

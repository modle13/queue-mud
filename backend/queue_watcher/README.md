# Usage with Pipenv

```
pipenv --python 3.7
pipenv install
watch 'pipenv shell python watch_queues.py'
```

# Usage with virtualenv

```
virtualenv -p python3.7 venv
. /venv/bin/activate
pip install requests
watch 'pipenv shell python watch_queues.py'
```

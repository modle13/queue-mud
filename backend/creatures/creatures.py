import os
import pika
import time

from functools import partial
from queue_modules.consumer import Consumer
from queue_modules.publisher import publish

EXCHANGE = 'creatures'
QUEUE = 'creatures'

creatures = [
    {'name': 'feral platypus', 'hp': 10}
]

ATTACKED = 'attacked {} with ferocity'
NOT_FOUND = 'there is no {} nearby'

class CreatureHandler():

    def __init__(self):
        print ('loading CreatureHandler')
        print ('creatures: {}'.format(creatures))


    # self is a bit confusing here
    def on_message(self, channel, delivery, creature):
        print("attacking {}".format(creature))

        resolved_attack = self.attack(creature.decode('utf-8'))
        publish(resolved_attack, 'responses')
        channel.basic_ack(delivery.delivery_tag)


    def attack(self, target):
        matched = next((ATTACKED for item in creatures if item["name"] == target), NOT_FOUND)
        return matched.format(target)


if __name__ == '__main__':
    consumer = Consumer(EXCHANGE, QUEUE, CreatureHandler())

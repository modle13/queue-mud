import os
import pika
import time

from functools import partial
from queue_modules.consumer import Consumer
from queue_modules.publisher import publish

EXCHANGE = 'commands'
QUEUE = 'commands'


class CommandHandler():
    
    def __init__(self):
        print ('loading CommandHandler')


    # self is a bit confusing here
    def on_message(self, channel, delivery, message):
        print('processing command: {}'.format(message))

        message = message.decode('utf-8')
        split_command = message.split(' ', 1)
        if split_command[0] == 'attack':
            try:
                target = split_command[1]
                publish(target, 'creatures')
            except IndexError:
                publish('attack what?', 'responses')
        else:
            publish('invalid command <<{}>>'.format(message), 'responses')

        channel.basic_ack(delivery.delivery_tag)


if __name__ == '__main__':
    consumer = Consumer(EXCHANGE, QUEUE, CommandHandler())

from flask_cors import CORS
from flask import Flask, jsonify, request

import json
import os
import pika
import sys
import time

from publisher import publish
from modules.status import get_status

EXCHANGE = 'commands'

def make_app():
    app = Flask(__name__)
    CORS(app)

    @app.route('/command', methods=['POST'])
    def add_command():
        print("Request json is {}".format(request.get_json()))
        command = request.get_json()["command"]
        # command is str at this point, but gets converted to bytes when published
        publish(command, EXCHANGE)
        return jsonify("command <<{}>> published".format(command))

    @app.route('/status')
    def status():
        return get_status()

    def get_random_quote():
        with open("status_responses.txt", "r") as f:
            lines = f.readlines()
            return random.choice(lines)

    return app


if __name__ == '__main__':
    app = make_app()
    app.run(port=8000)

import os
import pika
import time

from functools import partial
from queue_modules.consumer import Consumer
from queue_modules.publisher import publish

EXCHANGE = 'responses'
QUEUE = 'responses'


class ResponseHandler():

    def __init__(self):
        print ('loading CreatureHandler')


    # self is a bit confusing here
    def on_message(self, channel, delivery, message):
        print ('writing response')
        f = open("responselogs/responses.log", "a+")
        f.write('{}\n'.format(message.decode('utf-8')))
        f.close()
        channel.basic_ack(delivery.delivery_tag)


if __name__ == '__main__':
    consumer = Consumer(EXCHANGE, QUEUE, ResponseHandler())

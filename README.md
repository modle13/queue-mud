# prerequisites

- docker-ce
- docker-compose

# optional tools

- pipenv, for `backend/queue_watcher`
- tmuxinator, via gem install; https://github.com/tmuxinator/tmuxinator
    - (the version in the Ubuntu repos is outdated and does not support loading a profile from a file)
        ```
        gem install tmuxinator
        ```

# run with tmuxinator

```
tmuxinator start -p tmuxinator.yml
```

# run manually

### start docker-compose

```
docker-compose up --build
```

### test the endpoints

```
source verify.sh
```

### watch the queues

Follow the `README.md` in `backend/queue_watcher/`

### tail the response log

```
tail -f responselogs/responses.log
```

# troubleshooting

### remove all docker containers

```
#!/bin/bash

docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
docker rmi $(docker images -q) -f
```

### rebuild

```
docker-compose up --build
```

### follow docker-compose logs

this is the same output `docker-compose up` outputs if you don't detach from the process

(`servicename` = `rabbitmq`, `ingester`, etc.; this is not the `container_name`, though they may have the same value)

```
docker-compose logs -f servicename
```
